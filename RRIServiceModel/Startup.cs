using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RRIServiceModel.Models;

namespace RRIServiceModel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /// postgres
            services.AddDbContext<AppModelDbContext>(options => options.UseNpgsql(Configuration["ConnectionString:AppModelDbContext"]));

            /// sql server
           // services.AddDbContext<RRIServiceMedelDbContext>(option => option.UseSqlServer(Configuration["ConnectionString:RRIServiceMedel"]));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
        
        }
    }
}
