﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AppModelCore.Models.ClassContext
{
	public class banner_category
	{
		[Key]
		[Column(Order = 0)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public Guid banner_id { get; set; }
		public Guid category_id { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public string is_active { get; set; }

	}
}
