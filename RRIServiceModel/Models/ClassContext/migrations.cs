﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class migrations
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public string migration { get; set; }
		public int batch { get; set; }



	}
}
