﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class promotion
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public string code { get; set; }
		public string discount { get; set; }
		public string status { get; set; }
		public string type { get; set; }
		public Guid order_id { get; set; }
		public string id_promotion_qchang { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

	}
}
