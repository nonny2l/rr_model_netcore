﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class home_service
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public string service_url { get; set; }
		public string title_th { get; set; }
		public string title_en { get; set; }
		public string description_th { get; set; }
		public string description_en { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public bool publish_status { get; set; }
		public Guid created_by { get; set; }
		public Guid updated_by { get; set; }

	}
}
