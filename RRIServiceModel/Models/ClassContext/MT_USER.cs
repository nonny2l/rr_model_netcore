﻿namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_USER : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int USER_ID { get; set; }

        [Column(Order = 1)]
        [StringLength(60)]
        public string USERNAME { get; set; }

        [Column(Order = 2)]
        [StringLength(60)]
        public string PASSWORD { get; set; }

        [StringLength(100)]
        public string REMEMBER_TOKEN { get; set; }

        public int USER_TYPE_ID { get; set; }

        [Column(Order = 3)]
        public int? USER_GROUP_ID { get; set; }

        [StringLength(10)]
        public string EMP_NO { get; set; }

        [StringLength(10)]
        public string AGENT_NO { get; set; }

        [StringLength(255)]
        public string NAME { get; set; }

        [StringLength(255)]
        public string AVATAR { get; set; }

        [StringLength(255)]
        public string EMAIL { get; set; }
        [StringLength(1000)]
        public string REMARK { get; set; }
    }
}
