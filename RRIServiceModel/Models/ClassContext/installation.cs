﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class installation
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public Guid user_id { get; set; }
		public string device_token { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string device_type { get; set; }
		public string device_uuid { get; set; }
		public string passcode { get; set; }
		public string lang { get; set; }

	}
}
