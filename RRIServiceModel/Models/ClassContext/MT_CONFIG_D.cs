﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class mt_config_d
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int config_d_id { get; set; }
        public string config_d_code { get; set; }
        public int config_d_seq { get; set; }
        public string config_d_tname { get; set; }
        public string config_d_ename { get; set; }
        public int parent_d_id { get; set; }
        public string is_active { get; set; }
        public string create_by { get; set; }
        public DateTime? create_date { get; set; }
        public string update_by { get; set; }
        public DateTime? update_date { get; set; }
        public string delete_by { get; set; }
        public DateTime? delete_date { get; set; }
        public string config_h_code { get; set; }



    }
}
