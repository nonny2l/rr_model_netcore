﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class mt_config_h
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int config_h_id { get; set; }
		public string config_h_code { get; set; }
        public string config_h_tname { get; set; }
        public string config_h_ename { get; set; }
        public string is_active { get; set; }
        public string create_by { get; set; }
        public DateTime? create_date { get; set; }
        public string update_by { get; set; }
        public DateTime? update_date { get; set; }
        public string delete_by { get; set; }
        public DateTime? delete_date { get; set; }



    }
}
