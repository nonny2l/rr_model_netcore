﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
	public class sku_config
	{

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }

		[StringLength(60)]
		public string sku_code { get; set; }

		[StringLength(4000)]
		public string title_th { get; set; }

		[StringLength(4000)]
		public string title_en { get; set; }
		public string qchang_id { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? update_at { get; set; }
		public Guid created_by { get; set; }
		public Guid update_by { get; set; }

	}
}
