namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_USER_TYPE : MasterModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int USER_TYPE_ID { get; set; }

        [StringLength(255)]
        public string USER_TYPE_NAME { get; set; }


    }
}
