﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class failed_jobs
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public string connection { get; set; }
		public string queue { get; set; }
		public string payload { get; set; }
		public string exception { get; set; }
		public DateTime? failed_at { get; set; }
	}
}
