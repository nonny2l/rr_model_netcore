namespace RRPlatFormModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class LOG_API
    {
        [Key]
        public long ID { get; set; }
        [Column(TypeName = "text")] 
        public string REQUEST { get; set; }
        [Column(TypeName = "text")] 
        public string RESPONSE { get; set; }
        [StringLength(50)]
        public string STATUS { get; set; }
        public DateTime? REQUEST_DATETIME { get; set; }
        public DateTime? RESPONSE_DATETIME { get; set; }
        [StringLength(100)]
        public string MODULE_TYPE { get; set; }
        [StringLength(100)]
        public string ACTION_TYPE { get; set; }
        [StringLength(100)]
        public string USER_ACCOUNT { get; set; }
        [StringLength(50)]
        public string IP { get; set; }
        [StringLength(30)]
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        [StringLength(30)]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(30)]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
        [StringLength(100)]
        public string TRANSACTION_ID { get; set; }
    }
}
