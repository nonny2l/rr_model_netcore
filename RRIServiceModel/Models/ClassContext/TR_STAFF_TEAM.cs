﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class TR_STAFF_TEAM : MasterModel
    {
        [Key]
        public int STAFF_TEAM_ID { get; set; }
        public int USER_ID { get; set; }
        public int? PARENT_ID { get; set; }
        public string USER_NAME { get; set; }
    }
}
