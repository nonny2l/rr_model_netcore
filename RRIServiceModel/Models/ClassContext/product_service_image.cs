﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class product_service_image
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public Guid product_service_id { get; set; }
		public string url_image { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public bool flag { get; set; }

		public string is_active { get; set; }
	}
}
