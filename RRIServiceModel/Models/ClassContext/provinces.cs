﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class provinces
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public int code { get; set; }
		public string name_in_th { get; set; }
		public string name_in_en { get; set; }
		public bool flag_open { get; set; }

		public string is_active { get; set; }

	}
}
