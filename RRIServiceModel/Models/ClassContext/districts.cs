﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class districts
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public int code { get; set; }
		public string name_th { get; set; }
		public string name_en { get; set; }
		public string province_id { get; set; }
	}
}
