﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class user_address
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public Guid user_id { get; set; }
		public string address { get; set; }
		public int province_id { get; set; }
		public int district_id { get; set; }
		public int sub_district_id { get; set; }
		public bool flag_default { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public bool flag_delete { get; set; }

	}
}
