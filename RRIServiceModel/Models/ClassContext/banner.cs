﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AppModelCore.Models.ClassContext
{
	public class banner
	{

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }

		public string banner_url { get; set; }

		public DateTime? start_date_time { get; set; }
		public DateTime? end_date_time { get; set; }
		public bool publish_status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public Guid created_by { get; set; }
		public Guid updated_by { get; set; }
		public string ratio { get; set; }
		public string url { get; set; }
		public string title_th { get; set; }
		public string title_en { get; set; }
		public string product_type { get; set; }

		public string is_active { get; set; }

	}
}
