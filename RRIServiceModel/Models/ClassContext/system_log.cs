﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class system_log
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public string type { get; set; }
		public string subject { get; set; }
		public Guid subject_id { get; set; }
		public Guid by_user { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string content_log { get; set; }

	}
}
