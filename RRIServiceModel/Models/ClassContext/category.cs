﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class category
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid id { get; set; }
		public string title_th { get; set; }
		public string title_en { get; set; }
		public string icon_pic { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public Guid created_by { get; set; }
		public Guid updated_by { get; set; }
		public string qchang_cate_id { get; set; }
		public string cate_banner_img { get; set; }
		public string description_th { get; set; }
		public string description_en { get; set; }
		public int ordering { get; set; }
		public string content_id { get; set; }
		public string is_active { get; set; }

	}
}
