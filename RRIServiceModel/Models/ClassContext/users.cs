﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
	public class users
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public string name { get; set; }
		public string email { get; set; }
		public DateTime? email_verified_at { get; set; }
		public string password { get; set; }
		public string remember_token { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public string profile_picture { get; set; }
		public Int32 role { get; set; }
		public string user_device { get; set; }
		public string phone { get; set; }
        public string ca_no { get; set; }
        public string id_card { get; set; }

        public string lang { get; set; }
        public bool notification { get; set; }
        public bool pea_customer { get; set; }
    }
}
