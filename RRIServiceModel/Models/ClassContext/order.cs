﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class order
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid id { get; set; }
        public Guid customer_id { get; set; }
        public DateTime expected_date { get; set; }
        public Guid promotion_id { get; set; }
        public bool request_tax_invoice { get; set; }
        public string tax_invoice { get; set; }
        public string code { get; set; }
        public DateTime created_date { get; set; }
        public string status { get; set; }
        public Guid payment_id { get; set; }
        public DateTime? updated_at { get; set; }
        public DateTime? created_at { get; set; }
        public Guid user_address_id { get; set; }
        public Guid tax_invoice_adress_id { get; set; }
        public float price_float { get; set; }
        public float total_float4 { get; set; }
        public string promotion_code { get; set; }
        public int expected_date_period { get; set; }
        public float discount_float4 { get; set; }
        public string qchang_id { get; set; }
        public string tax_id { get; set; }
        public string tax_name { get; set; }
        public string quantity { get; set; }
        public string payment_order_ref_code { get; set; }
        public int peacare_running_no { get; set; }
        public int review_status { get; set; }
        public int review_rate { get; set; }
        public string review_message { get; set; }
        public int app_status { get; set; }
        public int content { get; set; }
        public int recommend { get; set; }
        public string note { get; set; }
        public DateTime? date_complete { get; set; }
        public int count_in_progress { get; set; }
        public bool flag_delete { get; set; }

    }
}
