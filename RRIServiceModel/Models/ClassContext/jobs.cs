﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class jobs
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public string queue { get; set; }
		public string payload { get; set; }
		public int attempts { get; set; }
		public int reserved_at { get; set; }
		public int available_at { get; set; }
		public int created_at { get; set; }


	}
}
