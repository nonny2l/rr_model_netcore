﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class provinces_product
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public int provinces_id { get; set; }
		public Guid product_id { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }

		public string is_active { get; set; }

	}
}
