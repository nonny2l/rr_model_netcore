﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class items
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public string product_id { get; set; }
		public string product_name { get; set; }
		public string type_of_work { get; set; }
		public decimal price { get; set; }
		public decimal total_price { get; set; }
		public int qty { get; set; }
		public string product_type { get; set; }
		public string sku_mapping { get; set; }
		public DateTime? updated_at { get; set; }
		public DateTime? created_at { get; set; }
		public Guid order_id { get; set; }
		public string ref_code { get; set; }
		public Guid id_product_service { get; set; }

	}
}
