﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class subdistricts
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public int code { get; set; }
		public string name_th { get; set; }
		public string name_en { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public int district_id { get; set; }
		public int zip_code { get; set; }

	}
}
