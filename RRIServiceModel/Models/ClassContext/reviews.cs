﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class reviews
	{
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid id { get; set; }
		public Guid order_id { get; set; }
		public string installation_after_files { get; set; }
		public string installation_after_remark { get; set; }
		public string installation_before_files { get; set; }
		public string installation_before_remark { get; set; }
		public string survey_files { get; set; }
		public string survey_remark { get; set; }
		public DateTime? updated_at { get; set; }
		public DateTime? created_at { get; set; }

	}
}
