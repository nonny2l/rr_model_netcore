﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppModelCore.Models.ClassContext
{
    public class broadcast_home
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid id { get; set; }
		public string title_th { get; set; }
		public string title_en { get; set; }
		public string description_th { get; set; }
		public string description_en { get; set; }
		public string broadcast_pic { get; set; }
		public DateTime? start_date_time { get; set; }
		public DateTime? end_date_time { get; set; }
		public string content_id { get; set; }
		public bool publish_status { get; set; }
		public DateTime? created_at { get; set; }
		public DateTime? updated_at { get; set; }
		public Guid created_by { get; set; }
		public Guid updated_by { get; set; }
		public Guid category_id { get; set; }
	}
}
