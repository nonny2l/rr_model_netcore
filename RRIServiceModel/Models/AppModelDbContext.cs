﻿using AppModelCore.Models.ClassContext;
using Microsoft.EntityFrameworkCore;
using RRPlatFormModel.Models;

namespace RRIServiceModel.Models
{
    public class AppModelDbContext : DbContext
    {
        public AppModelDbContext(DbContextOptions<AppModelDbContext> option)
        : base(option)
        { }

        public DbSet<LOG_API> LOG_API { get; set; }
        public DbSet<MT_USER> MT_USER { get; set; }
        public DbSet<MT_USER_TYPE> MT_USER_TYPE { get; set; }
        public DbSet<banner> banner { get; set; }
        public DbSet<banner_category> banner_category { get; set; }
        public DbSet<broadcast> broadcast { get; set; }
        public DbSet<broadcast_home> broadcast_home { get; set; }
        public DbSet<category> category { get; set; }
        public DbSet<districts> districts { get; set; }
        public DbSet<failed_jobs> failed_jobs { get; set; }
        public DbSet<home_banner> home_banner { get; set; }
        public DbSet<home_service> home_service { get; set; }
        public DbSet<installation> installation { get; set; }
        public DbSet<items> items { get; set; }
        public DbSet<jobs> jobs { get; set; }
        public DbSet<migrations> migrations { get; set; }
        public DbSet<mt_config_d> mt_config_d { get; set; }
        public DbSet<mt_config_h> mt_config_h { get; set; }
        public DbSet<notification> notification { get; set; }
        public DbSet<order> order { get; set; }
        public DbSet<otp_timestamp> otp_timestamp { get; set; }
        public DbSet<pages> pages { get; set; }
        public DbSet<password_resets> password_resets { get; set; }
        public DbSet<payment_log> payment_log { get; set; }
        public DbSet<payments> payments { get; set; }
        public DbSet<product_service> product_service { get; set; }
        public DbSet<product_service_image> product_service_image { get; set; }
        public DbSet<promotion> promotion { get; set; }
        public DbSet<provinces> provinces { get; set; }
        public DbSet<provinces_product> provinces_product { get; set; }
        public DbSet<qchang_create_order_log> qchang_create_order_log { get; set; }
        public DbSet<qchang_update_status_log> qchang_update_status_log { get; set; }
        public DbSet<reviews> reviews { get; set; }
        public DbSet<subdistricts> subdistricts { get; set; }
        public DbSet<system_log> system_log { get; set; }
        public DbSet<user_address> user_address { get; set; }
        public DbSet<users> users { get; set; }
        public DbSet<sku_config> sku_config { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           // modelBuilder.HasPostgresExtension("uuid-ossp")
           //.Entity<users>()
           //.Property(e => e.id)
           //.HasDefaultValueSql("uuid_generate_v4()");
        }
    }
}
