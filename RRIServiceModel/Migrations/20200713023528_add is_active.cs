﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AppModelCore.Migrations
{
    public partial class addis_active : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "provinces_product",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "provinces",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "product_service_image",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "category",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "banner_category",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "banner",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_active",
                table: "provinces_product");

            migrationBuilder.DropColumn(
                name: "is_active",
                table: "provinces");

            migrationBuilder.DropColumn(
                name: "is_active",
                table: "product_service_image");

            migrationBuilder.DropColumn(
                name: "is_active",
                table: "category");

            migrationBuilder.DropColumn(
                name: "is_active",
                table: "banner_category");

            migrationBuilder.DropColumn(
                name: "is_active",
                table: "banner");
        }
    }
}
