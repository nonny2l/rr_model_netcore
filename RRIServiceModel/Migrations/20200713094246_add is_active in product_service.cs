﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AppModelCore.Migrations
{
    public partial class addis_activeinproduct_service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "is_active",
                table: "product_service",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_active",
                table: "product_service");
        }
    }
}
