﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AppModelCore.Migrations
{
    public partial class addalltable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "banner",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    banner_url = table.Column<string>(nullable: true),
                    start_date_time = table.Column<DateTime>(nullable: true),
                    end_date_time = table.Column<DateTime>(nullable: true),
                    publish_status = table.Column<bool>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false),
                    ratio = table.Column<string>(nullable: true),
                    url = table.Column<string>(nullable: true),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true),
                    product_type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_banner", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "banner_category",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    banner_id = table.Column<Guid>(nullable: false),
                    category_id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_banner_category", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "broadcast",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true),
                    description_th = table.Column<string>(nullable: true),
                    description_en = table.Column<string>(nullable: true),
                    banner_url = table.Column<string>(nullable: true),
                    alert_type = table.Column<string>(nullable: true),
                    start_date_time = table.Column<DateTime>(nullable: true),
                    end_date_time = table.Column<DateTime>(nullable: true),
                    publish_status = table.Column<bool>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false),
                    ratio = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_broadcast", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "broadcast_home",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true),
                    description_th = table.Column<string>(nullable: true),
                    description_en = table.Column<string>(nullable: true),
                    broadcast_pic = table.Column<string>(nullable: true),
                    start_date_time = table.Column<DateTime>(nullable: true),
                    end_date_time = table.Column<DateTime>(nullable: true),
                    content_id = table.Column<string>(nullable: true),
                    publish_status = table.Column<bool>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false),
                    category_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_broadcast_home", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "category",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true),
                    icon_pic = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false),
                    qchang_cate_id = table.Column<string>(nullable: true),
                    cate_banner_img = table.Column<string>(nullable: true),
                    description_th = table.Column<string>(nullable: true),
                    description_en = table.Column<string>(nullable: true),
                    ordering = table.Column<int>(nullable: false),
                    content_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "districts",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code = table.Column<int>(nullable: false),
                    name_th = table.Column<string>(nullable: true),
                    name_en = table.Column<string>(nullable: true),
                    province_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_districts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "failed_jobs",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    connection = table.Column<string>(nullable: true),
                    queue = table.Column<string>(nullable: true),
                    payload = table.Column<string>(nullable: true),
                    exception = table.Column<string>(nullable: true),
                    failed_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_failed_jobs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "home_banner",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    banner_url = table.Column<string>(nullable: true),
                    alert_type = table.Column<string>(nullable: true),
                    start_date_time = table.Column<DateTime>(nullable: true),
                    end_date_time = table.Column<DateTime>(nullable: true),
                    publish_status = table.Column<bool>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false),
                    ratio = table.Column<string>(nullable: true),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_home_banner", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "home_service",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    service_url = table.Column<string>(nullable: true),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true),
                    description_th = table.Column<string>(nullable: true),
                    description_en = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    publish_status = table.Column<bool>(nullable: false),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_home_service", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "installation",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    user_id = table.Column<Guid>(nullable: false),
                    device_token = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    device_type = table.Column<string>(nullable: true),
                    device_uuid = table.Column<string>(nullable: true),
                    passcode = table.Column<string>(nullable: true),
                    lang = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_installation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "items",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    product_id = table.Column<string>(nullable: true),
                    product_name = table.Column<string>(nullable: true),
                    type_of_work = table.Column<string>(nullable: true),
                    price = table.Column<decimal>(nullable: false),
                    total_price = table.Column<decimal>(nullable: false),
                    qty = table.Column<int>(nullable: false),
                    product_type = table.Column<string>(nullable: true),
                    sku_mapping = table.Column<string>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    order_id = table.Column<Guid>(nullable: false),
                    ref_code = table.Column<string>(nullable: true),
                    id_product_service = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_items", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "jobs",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    queue = table.Column<string>(nullable: true),
                    payload = table.Column<string>(nullable: true),
                    attempts = table.Column<int>(nullable: false),
                    reserved_at = table.Column<int>(nullable: false),
                    available_at = table.Column<int>(nullable: false),
                    created_at = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_jobs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "migrations",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    migration = table.Column<string>(nullable: true),
                    batch = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_migrations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "mt_config_d",
                columns: table => new
                {
                    config_d_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    config_d_code = table.Column<string>(nullable: true),
                    config_d_seq = table.Column<int>(nullable: false),
                    config_d_tname = table.Column<string>(nullable: true),
                    config_d_ename = table.Column<string>(nullable: true),
                    parent_d_id = table.Column<int>(nullable: false),
                    is_active = table.Column<string>(nullable: true),
                    create_by = table.Column<string>(nullable: true),
                    create_date = table.Column<DateTime>(nullable: true),
                    update_by = table.Column<string>(nullable: true),
                    update_date = table.Column<DateTime>(nullable: true),
                    delete_by = table.Column<string>(nullable: true),
                    delete_date = table.Column<DateTime>(nullable: true),
                    config_h_code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mt_config_d", x => x.config_d_id);
                });

            migrationBuilder.CreateTable(
                name: "mt_config_h",
                columns: table => new
                {
                    config_h_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    config_h_code = table.Column<string>(nullable: true),
                    config_h_tname = table.Column<string>(nullable: true),
                    config_h_ename = table.Column<string>(nullable: true),
                    is_active = table.Column<string>(nullable: true),
                    create_by = table.Column<string>(nullable: true),
                    create_date = table.Column<DateTime>(nullable: true),
                    update_by = table.Column<string>(nullable: true),
                    update_date = table.Column<DateTime>(nullable: true),
                    delete_by = table.Column<string>(nullable: true),
                    delete_date = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mt_config_h", x => x.config_h_id);
                });

            migrationBuilder.CreateTable(
                name: "notification",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    data = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    notification_type = table.Column<int>(nullable: false),
                    subject_key = table.Column<Guid>(nullable: false),
                    to_user_id = table.Column<Guid>(nullable: false),
                    read_status = table.Column<bool>(nullable: false),
                    flag_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "order",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    customer_id = table.Column<Guid>(nullable: false),
                    expected_date = table.Column<DateTime>(nullable: false),
                    promotion_id = table.Column<Guid>(nullable: false),
                    request_tax_invoice = table.Column<bool>(nullable: false),
                    tax_invoice = table.Column<string>(nullable: true),
                    code = table.Column<string>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    payment_id = table.Column<Guid>(nullable: false),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    user_address_id = table.Column<Guid>(nullable: false),
                    tax_invoice_adress_id = table.Column<Guid>(nullable: false),
                    price_float = table.Column<float>(nullable: false),
                    total_float4 = table.Column<float>(nullable: false),
                    promotion_code = table.Column<string>(nullable: true),
                    expected_date_period = table.Column<int>(nullable: false),
                    discount_float4 = table.Column<float>(nullable: false),
                    qchang_id = table.Column<string>(nullable: true),
                    tax_id = table.Column<string>(nullable: true),
                    tax_name = table.Column<string>(nullable: true),
                    quantity = table.Column<string>(nullable: true),
                    payment_order_ref_code = table.Column<string>(nullable: true),
                    peacare_running_no = table.Column<int>(nullable: false),
                    review_status = table.Column<int>(nullable: false),
                    review_rate = table.Column<int>(nullable: false),
                    review_message = table.Column<string>(nullable: true),
                    app_status = table.Column<int>(nullable: false),
                    content = table.Column<int>(nullable: false),
                    recommend = table.Column<int>(nullable: false),
                    note = table.Column<string>(nullable: true),
                    date_complete = table.Column<DateTime>(nullable: true),
                    count_in_progress = table.Column<int>(nullable: false),
                    flag_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_order", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "otp_timestamp",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    phone_number = table.Column<string>(nullable: true),
                    otp = table.Column<string>(nullable: true),
                    timeout = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    is_verify = table.Column<bool>(nullable: false),
                    ref_code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_otp_timestamp", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "pages",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    alias = table.Column<string>(nullable: true),
                    content_en = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    content_th = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pages", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "password_resets",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    token = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_password_resets", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "payment_log",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    response = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payment_log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "payments",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    order_id = table.Column<Guid>(nullable: false),
                    data = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payments", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "product_service",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    title_th = table.Column<string>(nullable: true),
                    title_en = table.Column<string>(nullable: true),
                    description_th = table.Column<string>(nullable: true),
                    description_en = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    updated_by = table.Column<Guid>(nullable: false),
                    category_id = table.Column<Guid>(nullable: false),
                    content_id_by_qchang = table.Column<string>(nullable: true),
                    short_description_th = table.Column<string>(nullable: true),
                    short_description_en = table.Column<string>(nullable: true),
                    province = table.Column<string>(nullable: true),
                    province_status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_service", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "product_service_image",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    product_service_id = table.Column<Guid>(nullable: false),
                    url_image = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    flag = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_service_image", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "promotion",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    discount = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    order_id = table.Column<Guid>(nullable: false),
                    id_promotion_qchang = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_promotion", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "provinces",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code = table.Column<int>(nullable: false),
                    name_in_th = table.Column<string>(nullable: true),
                    name_in_en = table.Column<string>(nullable: true),
                    flag_open = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_provinces", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "provinces_product",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    provinces_id = table.Column<int>(nullable: false),
                    product_id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_provinces_product", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "qchang_create_order_log",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    order_id = table.Column<Guid>(nullable: false),
                    response = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_qchang_create_order_log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "qchang_update_status_log",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    qchang_id = table.Column<string>(nullable: true),
                    data = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_qchang_update_status_log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "reviews",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    order_id = table.Column<Guid>(nullable: false),
                    installation_after_files = table.Column<string>(nullable: true),
                    installation_after_remark = table.Column<string>(nullable: true),
                    installation_before_files = table.Column<string>(nullable: true),
                    installation_before_remark = table.Column<string>(nullable: true),
                    survey_files = table.Column<string>(nullable: true),
                    survey_remark = table.Column<string>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reviews", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "subdistricts",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code = table.Column<int>(nullable: false),
                    name_th = table.Column<string>(nullable: true),
                    name_en = table.Column<string>(nullable: true),
                    latitude = table.Column<string>(nullable: true),
                    longitude = table.Column<string>(nullable: true),
                    district_id = table.Column<int>(nullable: false),
                    zip_code = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subdistricts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "system_log",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    type = table.Column<string>(nullable: true),
                    subject = table.Column<string>(nullable: true),
                    subject_id = table.Column<Guid>(nullable: false),
                    by_user = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    content_log = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_system_log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user_address",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    user_id = table.Column<Guid>(nullable: false),
                    address = table.Column<string>(nullable: true),
                    province_id = table.Column<int>(nullable: false),
                    district_id = table.Column<int>(nullable: false),
                    sub_district_id = table.Column<int>(nullable: false),
                    flag_default = table.Column<bool>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    flag_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_address", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    email_verified_at = table.Column<DateTime>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    remember_token = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    profile_picture = table.Column<string>(nullable: true),
                    role = table.Column<int>(nullable: false),
                    user_device = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    ca_no = table.Column<string>(nullable: true),
                    id_card = table.Column<string>(nullable: true),
                    lang = table.Column<string>(nullable: true),
                    notification = table.Column<bool>(nullable: false),
                    pea_customer = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "banner");

            migrationBuilder.DropTable(
                name: "banner_category");

            migrationBuilder.DropTable(
                name: "broadcast");

            migrationBuilder.DropTable(
                name: "broadcast_home");

            migrationBuilder.DropTable(
                name: "category");

            migrationBuilder.DropTable(
                name: "districts");

            migrationBuilder.DropTable(
                name: "failed_jobs");

            migrationBuilder.DropTable(
                name: "home_banner");

            migrationBuilder.DropTable(
                name: "home_service");

            migrationBuilder.DropTable(
                name: "installation");

            migrationBuilder.DropTable(
                name: "items");

            migrationBuilder.DropTable(
                name: "jobs");

            migrationBuilder.DropTable(
                name: "migrations");

            migrationBuilder.DropTable(
                name: "mt_config_d");

            migrationBuilder.DropTable(
                name: "mt_config_h");

            migrationBuilder.DropTable(
                name: "notification");

            migrationBuilder.DropTable(
                name: "order");

            migrationBuilder.DropTable(
                name: "otp_timestamp");

            migrationBuilder.DropTable(
                name: "pages");

            migrationBuilder.DropTable(
                name: "password_resets");

            migrationBuilder.DropTable(
                name: "payment_log");

            migrationBuilder.DropTable(
                name: "payments");

            migrationBuilder.DropTable(
                name: "product_service");

            migrationBuilder.DropTable(
                name: "product_service_image");

            migrationBuilder.DropTable(
                name: "promotion");

            migrationBuilder.DropTable(
                name: "provinces");

            migrationBuilder.DropTable(
                name: "provinces_product");

            migrationBuilder.DropTable(
                name: "qchang_create_order_log");

            migrationBuilder.DropTable(
                name: "qchang_update_status_log");

            migrationBuilder.DropTable(
                name: "reviews");

            migrationBuilder.DropTable(
                name: "subdistricts");

            migrationBuilder.DropTable(
                name: "system_log");

            migrationBuilder.DropTable(
                name: "user_address");

            migrationBuilder.DropTable(
                name: "users");
        }
    }
}
