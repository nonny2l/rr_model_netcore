﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AppModelCore.Migrations
{
    public partial class addlogtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LOG_API",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    REQUEST = table.Column<string>(type: "text", nullable: true),
                    RESPONSE = table.Column<string>(type: "text", nullable: true),
                    STATUS = table.Column<string>(maxLength: 50, nullable: true),
                    REQUEST_DATETIME = table.Column<DateTime>(nullable: true),
                    RESPONSE_DATETIME = table.Column<DateTime>(nullable: true),
                    MODULE_TYPE = table.Column<string>(maxLength: 100, nullable: true),
                    ACTION_TYPE = table.Column<string>(maxLength: 100, nullable: true),
                    USER_ACCOUNT = table.Column<string>(maxLength: 100, nullable: true),
                    IP = table.Column<string>(maxLength: 50, nullable: true),
                    CREATE_BY = table.Column<string>(maxLength: 30, nullable: true),
                    CREATE_DATE = table.Column<DateTime>(nullable: true),
                    UPDATE_BY = table.Column<string>(maxLength: 30, nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: true),
                    DELETE_BY = table.Column<string>(maxLength: 30, nullable: true),
                    DELETE_DATE = table.Column<DateTime>(nullable: true),
                    TRANSACTION_ID = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LOG_API", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MT_USER",
                columns: table => new
                {
                    USER_ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IS_ACTIVE = table.Column<string>(maxLength: 1, nullable: false),
                    CREATE_BY = table.Column<string>(maxLength: 20, nullable: false),
                    CREATE_DATE = table.Column<DateTime>(nullable: true),
                    UPDATE_BY = table.Column<string>(maxLength: 20, nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: true),
                    DELETE_BY = table.Column<string>(maxLength: 20, nullable: true),
                    DELETE_DATE = table.Column<DateTime>(nullable: true),
                    USERNAME = table.Column<string>(maxLength: 60, nullable: true),
                    PASSWORD = table.Column<string>(maxLength: 60, nullable: true),
                    REMEMBER_TOKEN = table.Column<string>(maxLength: 100, nullable: true),
                    USER_TYPE_ID = table.Column<int>(nullable: false),
                    USER_GROUP_ID = table.Column<int>(nullable: true),
                    EMP_NO = table.Column<string>(maxLength: 10, nullable: true),
                    AGENT_NO = table.Column<string>(maxLength: 10, nullable: true),
                    NAME = table.Column<string>(maxLength: 255, nullable: true),
                    AVATAR = table.Column<string>(maxLength: 255, nullable: true),
                    EMAIL = table.Column<string>(maxLength: 255, nullable: true),
                    REMARK = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MT_USER", x => x.USER_ID);
                });

            migrationBuilder.CreateTable(
                name: "MT_USER_TYPE",
                columns: table => new
                {
                    USER_TYPE_ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IS_ACTIVE = table.Column<string>(maxLength: 1, nullable: false),
                    CREATE_BY = table.Column<string>(maxLength: 20, nullable: false),
                    CREATE_DATE = table.Column<DateTime>(nullable: true),
                    UPDATE_BY = table.Column<string>(maxLength: 20, nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: true),
                    DELETE_BY = table.Column<string>(maxLength: 20, nullable: true),
                    DELETE_DATE = table.Column<DateTime>(nullable: true),
                    USER_TYPE_NAME = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MT_USER_TYPE", x => x.USER_TYPE_ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LOG_API");

            migrationBuilder.DropTable(
                name: "MT_USER");

            migrationBuilder.DropTable(
                name: "MT_USER_TYPE");
        }
    }
}
