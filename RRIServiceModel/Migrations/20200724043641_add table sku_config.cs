﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AppModelCore.Migrations
{
    public partial class addtablesku_config : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "sku_config",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sku_code = table.Column<string>(maxLength: 60, nullable: true),
                    title_th = table.Column<string>(maxLength: 4000, nullable: true),
                    title_en = table.Column<string>(maxLength: 4000, nullable: true),
                    qchang_id = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    update_at = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<Guid>(nullable: false),
                    update_by = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sku_config", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sku_config");
        }
    }
}
